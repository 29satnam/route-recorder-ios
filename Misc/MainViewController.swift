//
//  MainViewController.swift
//  BigCeterButtonTabbar
//
//  Created by Rostyslav Druzhchenko on 10/19/17.
//  Copyright © 2017 Rostyslav Druzhchenko. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {
  
    @IBOutlet weak var startActivityBtn: UIButton!
    @IBOutlet var rippleImg: UIImageView!
    
    @IBOutlet weak var mainTabBar: MainMenuTabbar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   mainTabBar!.items![2].isEnabled = false
    
     //   setupRippleBtn()
     //   setupStrtActivityBtn()
  }
    
    fileprivate func setupRippleBtn() {
        
        tabBar.addSubview(rippleImg)
        rippleImg.translatesAutoresizingMaskIntoConstraints = false
        rippleImg.centerXAnchor.constraint(equalTo: tabBar.centerXAnchor).isActive = true
        let verticalConstraint = NSLayoutConstraint(item: self.rippleImg, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.tabBar, attribute: NSLayoutAttribute.centerY, multiplier: 0.7, constant: 0)
        NSLayoutConstraint.activate([verticalConstraint])
        
        UIView.animate(withDuration: 1, delay: 1, options: [.repeat, .curveEaseOut], animations: {
            self.rippleImg.alpha = 0.0
        }, completion: nil)
        
       // let customTabbar = tabBar as! MainMenuTabbar
       // customTabbar.centerButton = rippleImg
        
    }
  
  fileprivate func setupStrtActivityBtn() {
    tabBar.addSubview(startActivityBtn)
    startActivityBtn.translatesAutoresizingMaskIntoConstraints = false
    startActivityBtn.centerXAnchor.constraint(equalTo: tabBar.centerXAnchor).isActive = true
    let verticalConstraint = NSLayoutConstraint(item: self.startActivityBtn, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.tabBar, attribute: NSLayoutAttribute.centerY, multiplier: 0.7, constant: 0)
    NSLayoutConstraint.activate([verticalConstraint])
    let customTabbar = tabBar as! MainMenuTabbar
    customTabbar.centerButton = startActivityBtn
  }
  
  @IBAction fileprivate func unwindToMainViewController(_ segue: UIStoryboardSegue) {}
  
}
