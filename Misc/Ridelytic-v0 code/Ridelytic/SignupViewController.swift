//
//  SignupViewController.swift
//  Ridelytic
//
//  Created by MyCandy on 17/07/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


class SignupViewController: UIViewController, UITextFieldDelegate, URLSessionDelegate, URLSessionTaskDelegate {

    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var activeTextField: UITextField?
    
    @IBOutlet weak var jEmailTF: UITextField!
    @IBOutlet weak var jUsernameTF: UITextField!
    @IBOutlet weak var jPasswordTF: UITextField!
    @IBOutlet weak var signupBtn: UIButtonDeviceClass!
    @IBOutlet weak var signupIndicator: NVActivityIndicatorView!
    //@IBOutlet weak var usrnamChkIndicator: NVActivityIndicatorView!
    //@IBOutlet weak var emailChkIndicator: NVActivityIndicatorView!
    
    
    @IBOutlet var emailAvlblImg: UIImageView!
    @IBOutlet var usrnameAvlblImg: UIImageView!
    @IBOutlet var passwordAvlblImg: UIImageView!

    
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        jEmailTF.delegate = self
        jUsernameTF.delegate = self
        jPasswordTF.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        self.hideKeyboard()
        assignbackground()
        
        DispatchQueue.main.async(execute: {
            //self.usrnamChkIndicator.stopAnimating()
            //self.usrnameAvlblImg.isHidden = true
            self.usrnameAvlblImg.image = nil
        })
    }

    @objc func getHintsFromTextField(textField: UITextField) {
        print("Hints for textField: \(textField)")
    }

    @IBAction func emailDidChanged(_ sender: UITextField) {
        //Todo: Check if email is valid or availble once user stops typing
        /*
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector:#selector(SignupViewController.getHintsFromTextField), object: sender)
        self.perform(#selector(SignupViewController.getHintsFromTextField), with: sender,
                     afterDelay: 1.0)
        */
        if isValidEmail(str: jEmailTF.text!) == true && jEmailTF.text!.isEmpty == false {
            print("Yass")
            self.emailAvlblImg.image = #imageLiteral(resourceName: "available")
            
        } else if isValidEmail(str: jEmailTF.text!) == false && jEmailTF.text!.isEmpty == false {
            print("Nay")
            self.emailAvlblImg.image = #imageLiteral(resourceName: "notAvailable")
            
        } else if jEmailTF.text!.isEmpty == true {
            self.emailAvlblImg.image? = UIImage()
        }
        
    }
    
    @IBAction func usernameDidChanged(_ sender: UITextField) {
        
        if isValidUsername(str: jUsernameTF.text!) == true && jUsernameTF.text!.isEmpty == false {
            print("Yass")
            self.usrnameAvlblImg.image = #imageLiteral(resourceName: "available")

        } else if isValidUsername(str: jUsernameTF.text!) == false && jUsernameTF.text!.isEmpty == false {
            print("Nay")
            self.usrnameAvlblImg.image = #imageLiteral(resourceName: "notAvailable")
            
            
        } else if jUsernameTF.text!.isEmpty == true {
            print("Vacant")
            self.usrnameAvlblImg.image? = UIImage()
        }
        
    }
    
    
    @IBAction func passwordDidChanged(_ sender: UITextField) {
        
        if isValidPassword(str: jPasswordTF.text!) == true && jPasswordTF.text!.isEmpty == false {
            print("Yass")
            self.passwordAvlblImg.image = #imageLiteral(resourceName: "available")
            
        } else if isValidPassword(str: jPasswordTF.text!) == false && jPasswordTF.text!.isEmpty == false {
            print("Nay")
            self.passwordAvlblImg.image = #imageLiteral(resourceName: "notAvailable")
            
            
        } else if jPasswordTF.text!.isEmpty == true {
            print("Vacant")
            self.passwordAvlblImg.image? = UIImage()
        }
        
    }
    
    
    @IBAction func signupActionBtn(_ sender: UIButton) {
        //self.signupBtn.isUserInteractionEnabled = false
        animateButtonOnPress(button: signupBtn)
        if jUsernameTF.text == "" || jPasswordTF.text == "" {
            print("can't be empty")
            DispatchQueue.main.async(execute: {
                self.signupBtn.isUserInteractionEnabled = true
                let alertController = UIAlertController(title: "Warning!", message: "Username or password can't be left empty.", preferredStyle: .actionSheet)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true) { }
            })
        } else {
            if isConnectedToNetwork() == true {
                

                doSignup(username: jUsernameTF.text!, password: jPasswordTF.text!, email: jEmailTF.text!)


            } else {
                print("You're offline")
                self.signupIndicator.stopAnimating()
                DispatchQueue.main.async(execute: {
                    self.signupBtn.isUserInteractionEnabled = true
                    let alertController = UIAlertController(title: "Connectivity Issue", message: "Please connect to Internet and try again.", preferredStyle: .actionSheet)
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true) { }
                })
                
            }
        }
    }
    
    
    
    func doSignup(username: String, password: String, email: String) {
        
        let parameters: Parameters = [
            "username": "\(username)",
            "password": "\(password)",
            "email": "\(email)"
        ]
        
        Alamofire.request("https://cdn.ridelytic.io/riders/", method: .post, parameters: parameters, encoding: URLEncoding.default)
            .responseJSON { httpResponse in
                
                switch httpResponse.result {
                case .success(let dpdSuccess):

                    print("httpResponse.response?.statusCode:", httpResponse.response?.statusCode)
                    print("JSON(dpdSuccess)[\"id\"]:", JSON(dpdSuccess))

                    if (httpResponse.response?.statusCode == 200) && (JSON(dpdSuccess)["id"] != .null) {

                        self.navigationController?.popViewController(animated: true)
                        //self.performSegue(withIdentifier: "signedIn", sender: self)
                        DispatchQueue.main.async(execute: {
                            self.signupBtn.isUserInteractionEnabled = true
                            self.signupIndicator.stopAnimating()
                            print("Signed Up")
                        })
                        // TODO: Distinctively show "Username or email is already in use."
                    } else if (httpResponse.response?.statusCode == 401) && (JSON(dpdSuccess)["status"].number == 401) {
                        print("401 Unauthorized: The current session is not authorized to perform this action")
                        self.signupIndicator.stopAnimating()
                        print("lol:", (JSON(dpdSuccess)))
                        DispatchQueue.main.async(execute: {
                            self.signupBtn.isUserInteractionEnabled = true
                            let alertController = UIAlertController(title: "Please try again", message: "Username or email is already in use.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })
                    } else if (httpResponse.response?.statusCode == 204) {
                        print("204 (No Content), the request was successful, but there is no result.")
                        self.signupIndicator.stopAnimating()
                        DispatchQueue.main.async(execute: {
                            self.signupBtn.isUserInteractionEnabled = true

                            let alertController = UIAlertController(title: "Please try again.", message: "Request was successful, but there is no result.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })
                    } else if (httpResponse.response?.statusCode == 400) {
                        print("400 Bad Request: The request contained invalid data and could not be completed")
                        self.signupIndicator.stopAnimating()
                        self.signupBtn.isUserInteractionEnabled = true

                        DispatchQueue.main.async(execute: {
                            self.signupBtn.isUserInteractionEnabled = true

                            let alertController = UIAlertController(title: "Please try again.", message: "Request contained Invalid data and could not be completed.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })


                    } else if (httpResponse.response?.statusCode == 502) {
                        print("502 Bad Gateway")
                        self.signupIndicator.stopAnimating()

                        DispatchQueue.main.async(execute: {
                            self.signupBtn.isUserInteractionEnabled = true

                            let alertController = UIAlertController(title: "Bad Gateway", message: "Server error, please try again later.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })

                    } else {
                        print("Something unexpected happened")
                        self.signupIndicator.stopAnimating()

                        DispatchQueue.main.async(execute: {
                            self.signupBtn.isUserInteractionEnabled = true

                            let alertController = UIAlertController(title: "Please try again later.", message: "Something unexpected happened.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })

                    }
                case .failure(let failure):
                    print("failure,", JSON(failure))
                }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNotificationKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func assignbackground(){
        let background = UIImage(named: "cyclistBg")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        self.view.insertSubview(imageView, at: 0)
    }
    

    //MARK: Move Keyboard
    
    func setNotificationKeyboard ()  {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        activeTextField = nil
    }
    @objc func keyboardWasShown(notification: NSNotification) {
        print("shown")
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        
        if let activeField = self.activeTextField {
            if (!aRect.contains(activeField.frame.origin)) {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification) {
        
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0,0.0, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let nextField = textField.superview?.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
}
