//
//  Constants.swift
//  Ridelytic
//
//  Created by MyCandy on 20/07/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

import Foundation
import UIKit

var navBarColor = UIColor(red: 16/255, green: 16/255, blue: 16/255, alpha: 1.0)

let authPrefs:UserDefaults = UserDefaults.standard
let goalPrefs:UserDefaults = UserDefaults.standard
