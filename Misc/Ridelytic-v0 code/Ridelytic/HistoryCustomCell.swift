//
//  HistoryCustomCell.swift
//  Ridelytic
//
//  Created by MyCandy on 28/07/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

import Foundation
import UIKit

class CustomCell: UITableViewCell {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
}
