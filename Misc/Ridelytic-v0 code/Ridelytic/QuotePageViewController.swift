
import UIKit

class QuotePageViewController: UIViewController, UIPageViewControllerDataSource {
    
    @IBOutlet weak var quoteLabel: UILabelDeviceClass!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var pageViewController : UIPageViewController?
    var pageTitles : Array<String> = ["Ride when you want, how you want it.", "Map your ride with stats.", "Set daily goals and achieve."]
    var currentIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
        pageControl.numberOfPages = self.pageTitles.count
        pageControl.currentPage = 0
        
        
        pageViewController = UIPageViewController(transitionStyle: .pageCurl, navigationOrientation: .horizontal, options: nil)
        pageViewController!.dataSource = self
        
        
        let startingViewController: InstructionView = viewControllerAtIndex(index: 0)!
        let viewControllers = [startingViewController]
        pageViewController!.setViewControllers(viewControllers , direction: .forward, animated: false, completion: nil)
        pageViewController!.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height);
        
        addChildViewController(pageViewController!)
        view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParentViewController: self)
  }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! InstructionView).pageIndex
        
        if (index == 0) || (index == NSNotFound) {
            print("this:", index)

            return nil
        }
        
        index -= 1
        pageControl.currentPage = index
        
        return viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! InstructionView).pageIndex
        
        if index == NSNotFound {
            return nil
        }
        
        index += 1
        pageControl.currentPage = index

        
        if (index == self.pageTitles.count) {
            return nil
        }
        
        print(index)
        
        return viewControllerAtIndex(index: index)
    }
    
    func viewControllerAtIndex(index: Int) -> InstructionView? {
        if self.pageTitles.count == 0 || index >= self.pageTitles.count {
            return nil
        }
        
        let pageContentViewController = InstructionView()
        quoteLabel.text = pageTitles[index]
        pageContentViewController.pageIndex = index
        currentIndex = index
        
        return pageContentViewController
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.pageTitles.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int{
        return 0
    }
  
}
