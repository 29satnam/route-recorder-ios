//
//  LocationServiceDeleagte.swift
//  Ridelytic
//
//  Created by Satnam on 01/11/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

import UIKit
import CoreLocation

public class LocationService: NSObject, CLLocationManagerDelegate{
    
    public static var sharedInstance = LocationService()
    let locationManager: CLLocationManager
    var locationDataArray: [CLLocation]
    var useFilter: Bool
    
    //...
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var startDate: Date!
    
    var LastUpdate: Double = 0
    var traveledDistance: Double = 0
    var straightDistance: Double = 0
    
    override init() {
        locationManager = CLLocationManager()
        locationManager.distanceFilter = 10
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
        if #available(iOS 9.0, *) {
            locationManager.allowsBackgroundLocationUpdates = true
        } else {
            // Fallback on earlier versions
        }
        locationManager.pausesLocationUpdatesAutomatically = false
        locationDataArray = [CLLocation]()
        
        useFilter = true
        
        super.init()
        
        locationManager.delegate = self
        
    }
    
    
    func startUpdatingLocation() {
        
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        } else {
            //tell view controllers to show an alert
            showTurnOnLocationServiceAlert()
        }
    }
    
    
    
    //MARK: CLLocationManagerDelegate protocol methods
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        
        

        if startLocation == nil {
            startLocation = locations.first
        } else if let location = locations.last {
            // Main Shit
            
            
            
            var locationAdded: Bool
            print("New Location", lastLocation.distance(from: location))
            
            if lastLocation.distance(from: location) > 10 {
                locationDataArray.append(location)
                locationAdded = true
            } else {
                locationAdded = false
            }
            
            if locationAdded {
                notifiyDidUpdateLocation(newLocation: location)
            }
            
            self.traveledDistance += lastLocation.distance(from: location)
            self.straightDistance = Double(startLocation.distance(from: locations.last!))
            self.LastUpdate = lastLocation.distance(from: locations.last!)
            
            print("LastUpdate", lastLocation.distance(from: locations.last!))
            print("Traveled Distance:", traveledDistance)
            print("Straight Distance:", startLocation.distance(from: locations.last!))
            
        }
        lastLocation = locations.last
        
        
        
        
        
        
    }
    
    /*  func filterAndAddLocation(_ location: CLLocation) -> Bool{
     //        lastLocation = locations.last
     
     if lastLocation.distance(from: lastLocation!) > 10 {
     print("traveled quality is good enough.")
     locationDataArray.append(location)
     return true
     }
     
     
     
     
     
     
     let age = -location.timestamp.timeIntervalSinceNow
     print("age--", age)
     if age > 10 {
     print("Locaiton is old.")
     return false
     }
     
     if location.horizontalAccuracy < 0 {
     print("Latitidue and longitude values are invalid.")
     return false
     }
     
     if location.horizontalAccuracy > 100 {
     print("Accuracy is too low.")
     return false
     }
     
     print("Location quality is good enough.")
     locationDataArray.append(location)
     
     return true
     
     } */
    
    
    public func locationManager(_ manager: CLLocationManager,
                                didFailWithError error: Error){
        if (error as NSError).domain == kCLErrorDomain && (error as NSError).code == CLError.Code.denied.rawValue{
            //User denied your app access to location information.
            showTurnOnLocationServiceAlert()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didChangeAuthorization status: CLAuthorizationStatus){
        if status == .authorizedWhenInUse{
            //You can resume logging by calling startUpdatingLocation here
        }
    }
    
    func showTurnOnLocationServiceAlert(){
        NotificationCenter.default.post(name: Notification.Name(rawValue:"showTurnOnLocationServiceAlert"), object: nil)
    }
    
    func notifiyDidUpdateLocation(newLocation:CLLocation){
        NotificationCenter.default.post(name: Notification.Name(rawValue:"didUpdateLocation"), object: nil, userInfo: ["location" : newLocation])
    }
}


