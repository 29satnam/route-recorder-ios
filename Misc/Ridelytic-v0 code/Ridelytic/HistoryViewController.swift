//
//  HistoryViewController.swift
//  Ridelytic
//
//  Created by MyCandy on 26/07/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

import UIKit

var dayString: String = String()

class HistoryViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var historyData: [(days:String, distance:String, hours:String)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: UIFont(name: "SourceSansPro-SemiBold", size: 15)!]
        
        historyData.append(contentsOf: [(days: "FRI.21.07.17", distance: "45KM", hours: "3H"), (days: "SAT.22.07.17", distance: "47KM", hours: "3H"), (days: "SUN.23.07.17", distance: "49KM", hours: "3H"), (days: "MON.24.07.17", distance: "41KM", hours: "2H"), (days: "TUE.25.07.17", distance: "57KM", hours: "4H"), (days: "WED.26.07.17", distance: "45KM", hours: "3H"), (days: "THU.27.07.17", distance: "38KM", hours: "2H"), (days: "FRI.28.07.17", distance: "60KM", hours: "3H"), (days: "SAT.29.07.17", distance: "64KM", hours: "4H"), (days: "SUN.30.07.17", distance: "82KM", hours: "4H"), (days: "MON.31.07.17", distance: "89KM", hours: "4H"), (days: "TUE.01.08.17", distance: "45KM", hours: "3H"), (days: "WED.02.08.17", distance: "95KM", hours: "4H"), (days: "THU.22.07.17", distance: "45KM", hours: "3H")])
        
        self.tableView.rowHeight = 64
        self.tableView.reloadData()
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackOpaque
        self.navigationController?.navigationBar.barTintColor = UIColor(cgColor: navBarColor.cgColor)

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Mark: TableView
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath, "got tapped", historyData[indexPath.item].days)
        
        dayString = historyData[indexPath.item].days
        
        self.performSegue(withIdentifier: "dayDetail", sender: self)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "dayDetail") {
            let vc = segue.destination as! HistoryDetailsViewController
            vc.passedDayStr = dayString
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyData.count;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell: CustomCell = self.tableView.dequeueReusableCell(withIdentifier: "MainCell") as! CustomCell
        
        //cell = UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "reuseIdentifier")
        
        cell.dayLabel.text = historyData[indexPath.row].days
        cell.distanceLabel.text = historyData[indexPath.row].distance
        cell.hoursLabel.text = historyData[indexPath.row].hours
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
}
