//
//  SetGoalViewController.swift
//  Ridelytic
//
//  Created by MyCandy on 25/07/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

protocol SetGoalDelegate: class {
    func modalDismissed()
}

import UIKit

class SetGoalViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var setGoalPicker: UIPickerView!
    
    var goalArray = Array(1...100)
    var stringArray: [String] = [String]()
    
    weak var delegate: SetGoalDelegate?
    @IBOutlet var pickerView: UIPickerView!

    
    //Mark: Save Goal
    @IBAction func setGoalActionBtn(_ sender: UIButton) {
        let row = self.pickerView.selectedRow(inComponent: 0)

        goalPrefs.set(goalArray[row], forKey: "DAYGOAL")
        goalPrefs.synchronize()
        dismissModal()
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        let number = 500
        for num in 101...number {
            if num % 5 == 0 {
                goalArray.append(num)
            }
        }
        
        stringArray = goalArray.map {
            String($0)
        }
        
        setGoalPicker.delegate = self

        self.view.backgroundColor = .clear
        
        let visuaEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        visuaEffectView.frame = self.view.bounds
        visuaEffectView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.view.insertSubview(visuaEffectView, at: 0)
 
    }
    
    
    @IBAction func dismissView(_ sender: UIButton) {
        dismissModal()
    }

    func dismissModal() {
        UIView.animate(withDuration: 0.25) {
            self.setNeedsStatusBarAppearanceUpdate()
        }

        self.dismiss(animated: true, completion: nil)
        delegate?.modalDismissed()
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return stringArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return stringArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
}
