//
//  HistoryDetailsViewController.swift
//  Ridelytic
//
//  Created by MyCandy on 31/07/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

import UIKit

class HistoryDetailsViewController: UIViewController {

    var passedDayStr: String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "\(passedDayStr)"
        print(passedDayStr)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackOpaque
        self.navigationController?.navigationBar.barTintColor = UIColor(cgColor: navBarColor.cgColor)

        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: UIFont(name: "SourceSansPro-SemiBold", size: 15)!]
    }

    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }

}
