//
//  SignupViewController.swift
//  Ridelytic
//
//  Created by MyCandy on 17/07/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class LoginViewController: UIViewController, UITextFieldDelegate, NSURLConnectionDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var activeTextField: UITextField?
    @IBOutlet weak var jUsernameTF: UITextField!
    @IBOutlet weak var jPasswordTF: UITextField!
    @IBOutlet weak var loginIndicator: NVActivityIndicatorView!
    @IBOutlet weak var loginBtn: UIButtonDeviceClass!

    lazy var data = NSMutableData()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        jUsernameTF.delegate = self
        jPasswordTF.delegate = self
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.hideKeyboard()
        assignbackground()
    }
    
    @IBAction func loginActionBtn(_ sender: UIButton) {
        self.loginBtn.isUserInteractionEnabled = false
        animateButtonOnPress(button: loginBtn)
        if jUsernameTF.text == "" || jPasswordTF.text == "" {
            print("can't be empty")
            DispatchQueue.main.async(execute: {
                self.loginBtn.isUserInteractionEnabled = true
                let alertController = UIAlertController(title: "Warning!", message: "Username or password can't be left empty.", preferredStyle: .actionSheet)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true) { }
            })
        } else {
            if isConnectedToNetwork() == true {
                
                doLogin(username: jUsernameTF.text!, password: jPasswordTF.text!)
                self.loginBtn.isUserInteractionEnabled = true

               /* if isValidExp(str: jUsernameTF.text!) == true {
                    print("Yass")
                    doLogin(username: jUsernameTF.text!, password: jPasswordTF.text!)
                } else {
                    print("Nay")
                    DispatchQueue.main.async(execute: {
                        self.loginBtn.isUserInteractionEnabled = true
                        let alertController = UIAlertController(title: "Not a valid username", message: "Username must not contain symbol or emoji, except an underscore.", preferredStyle: .actionSheet)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true) { }
                    })
                } */
            } else {
                print("You're offline")
                self.loginIndicator.stopAnimating()
                DispatchQueue.main.async(execute: {
                    self.loginBtn.isUserInteractionEnabled = true
                    let alertController = UIAlertController(title: "Connectivity Issue", message: "Please connect your device to Internet and try again.", preferredStyle: .actionSheet)
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true) { }
                })
            }
        }
    }
    
    
    
    // TEXTFIELD
    @IBAction func usernameDidChanged(_ sender: UITextField) {
        if ((self.jUsernameTF.text!.characters.count) >= 16) {
            self.jUsernameTF.deleteBackward()
            DispatchQueue.main.async(execute: {
                let alertController = UIAlertController(title: "Oops!", message: "Maximum 15 characters are allowed.", preferredStyle: .actionSheet)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true) { }
            })
        } else {
        }
    }
    
    @IBAction func fbSignup(_ sender: UIButton) {
        
    }
    
    func doLogin(username: String, password: String) {
        
        loginIndicator.startAnimating()
        loginBtn.isUserInteractionEnabled = false
        
        let parameters: Parameters = [
            "username": "\(username)",
            "password": "\(password)"
        ]
        
        Alamofire.request("https://cdn.ridelytic.io/riders/login", method: .post, parameters: parameters, encoding: URLEncoding.default)
            .responseJSON { httpResponse in
                
                switch httpResponse.result {
                case .success(let dpdSuccess):
                    
                    print("httpResponse.response?.statusCode:", httpResponse.response?.statusCode)
                    print("JSON(dpdSuccess)[\"id\"]:", JSON(dpdSuccess)["status"])
                    
                    if (httpResponse.response?.statusCode == 200) && (JSON(dpdSuccess)["id"] != .null) {
                        
                        authPrefs.set(username, forKey: "USERNAME")
                        authPrefs.set(JSON(dpdSuccess)["id"].string, forKey: "SESSIONID")
                        authPrefs.set(1, forKey: "ISLOGGEDIN")
                        authPrefs.synchronize()
                        
                        self.performSegue(withIdentifier: "signedIn", sender: self)
                        DispatchQueue.main.async(execute: {
                            self.loginBtn.isUserInteractionEnabled = true
                            self.loginIndicator.stopAnimating()
                            print("Loggedin")
                        })
                        
                    } else if (httpResponse.response?.statusCode == 401) && (JSON(dpdSuccess)["status"].number == 401) {
                        print("401 Unauthorized: The current session is not authorized to perform this action")
                        self.loginIndicator.stopAnimating()
                        print("lol:", (JSON(dpdSuccess)))
                        DispatchQueue.main.async(execute: {
                            self.loginBtn.isUserInteractionEnabled = true
                            let alertController = UIAlertController(title: "Please try again", message: "You've entered wrong username or password.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })
                    } else if (httpResponse.response?.statusCode == 204) {
                        print("204 (No Content), the request was successful, but there is no result.")
                        self.loginIndicator.stopAnimating()
                        DispatchQueue.main.async(execute: {
                            self.loginBtn.isUserInteractionEnabled = true
                            
                            let alertController = UIAlertController(title: "Please try again.", message: "Request was successful, but there is no result.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })
                    } else if (httpResponse.response?.statusCode == 400) {
                        print("400 Bad Request: The request contained invalid data and could not be completed")
                        self.loginIndicator.stopAnimating()
                        self.loginBtn.isUserInteractionEnabled = true
                        
                        DispatchQueue.main.async(execute: {
                            self.loginBtn.isUserInteractionEnabled = true
                            
                            let alertController = UIAlertController(title: "Please try again.", message: "Request contained Invalid data and could not be completed.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })
                        
                        
                    } else if (httpResponse.response?.statusCode == 502) {
                        print("502 Bad Gateway")
                        self.loginIndicator.stopAnimating()
                        
                        DispatchQueue.main.async(execute: {
                            self.loginBtn.isUserInteractionEnabled = true
                            
                            let alertController = UIAlertController(title: "Bad Gateway", message: "Server error, please try again later.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })
                        
                    } else {
                        print("Something unexpected happened")
                        self.loginIndicator.stopAnimating()
                        
                        DispatchQueue.main.async(execute: {
                            self.loginBtn.isUserInteractionEnabled = true
                            
                            let alertController = UIAlertController(title: "Please try again later.", message: "Something unexpected happened.", preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true) { }
                        })
                        
                    }

                case .failure(let failure):
                    print("Failure lol,", JSON(failure))
                    //Unknows - Server Down

                    DispatchQueue.main.async(execute: {
                        self.loginBtn.isUserInteractionEnabled = true

                        let alertController = UIAlertController(title: "Server Failure", message: "Failed to Interact with server, please try again later.", preferredStyle: .actionSheet)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true) { }
                    })
                }
        }
    }
    
    
    
   // func connection
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNotificationKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func assignbackground(){
        let background = UIImage(named: "cyclistBg")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        self.view.insertSubview(imageView, at: 0)
    }
    
    
    //MARK: Move Keyboard
    
    func setNotificationKeyboard ()  {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        activeTextField = nil
    }
    @objc func keyboardWasShown(notification: NSNotification) {
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        
        if let activeField = self.activeTextField {
            if (!aRect.contains(activeField.frame.origin)) {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification) {
        
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0,0.0, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let nextField = textField.superview?.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    
    
    
}
