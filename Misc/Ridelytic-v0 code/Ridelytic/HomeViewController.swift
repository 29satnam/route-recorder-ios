//
//  HomeViewController.swift
//  Ridelytic
//
//  Created by MyCandy on 20/07/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation
import KBCScrollView
import TTSegmentedControl

class HomeViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate  {
    
    
    
    @IBOutlet weak var initMapView: MKMapView!
    @IBOutlet weak var mapView: MKMapView!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var draggableView: UIView!
    @IBOutlet weak var startWoBtn: UIButtonDeviceClass!
    @IBOutlet weak var segmentedControl: TTSegmentedControl!

    
    var accuracyRangeCircle: MKCircle?
    var polyline: MKPolyline?
    var isZooming: Bool?
    var isBlockingAutoZoom: Bool?
    var zoomBlockingTimer: Timer?
    var didInitialZoom: Bool?
    var isTabBarVisible = true
    var initalViewFrame: CGRect?
    var newTabBarSize: CGRect?
    var isStatusBarChanged = false
    


    
    @IBOutlet weak var speedLabel: UILabel!
    
    //Mark: StartWorkOut --------------------------------
    @IBAction func startWorkoutAct(_ sender: UIButton) {
        
        DispatchQueue.main.async(execute: {
            self.initMapView.slideOut(to: SimpleAnimationEdge.right, x: self.view.frame.width, y: 0, duration: 1.5, delay: 0, completion: nil)
            self.initMapView.removeFromSuperview()
            animateButtonOnPress(button: self.startWoBtn)

            self.view.height +=  (self.tabBarController?.tabBar.frame.height)!
            self.draggableView.y += (self.tabBarController?.tabBar.frame.height)!
            self.tabBarController?.tabBar.slideOut(to: SimpleAnimationEdge.right, x: 0, y: self.draggableView.frame.origin.y + (self.tabBarController?.tabBar.frame.height)!, duration: 1.5, delay: 0, completion: nil)
            
            //self.scrollView.isHidden = false
           // self.segmentedControl.isHidden = false
            self.segmentedControl.insertSubview(self.view, at: 0)

            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        })
    }
    
    //Mark: viewDidLoad --------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        segmentedControl.itemTitles = ["STATS", "COURSE", "SPLITS"]
        segmentedControl.allowChangeThumbWidth = false
        segmentedControl.hasBounceAnimation = false
        segmentedControl.selectItemAt(index: 1, animated: true)
        
        scrollView.isPagingEnabled = true

        let logo = UIImage(named: "navTitle")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        self.mapView.delegate = self
        self.mapView.showsUserLocation = false
        self.mapView.userTrackingMode = .follow
        
        self.accuracyRangeCircle = MKCircle(center: CLLocationCoordinate2D.init(latitude: 41.887, longitude: -87.622), radius: 50)
        self.mapView.add(self.accuracyRangeCircle!)
        
        self.didInitialZoom = false

        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.updateMap(_:)), name: Notification.Name(rawValue:"didUpdateLocation"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showTurnOnLocationServiceAlert(_:)), name: Notification.Name(rawValue:"showTurnOnLocationServiceAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(appWillEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    @objc func handleFrameResize(notification: NSNotification) {
        print("Statusbar:", UIApplication.shared.statusBarFrame.size.height)
        isStatusBarChanged = true
    }
    
    //Mark: viewDidAppear --------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       // scrollView.isHidden = true
       // segmentedControl.isHidden = true
        
        scrollView.removeFromSuperview()
        segmentedControl.removeFromSuperview()
        
        LocationService.sharedInstance.startUpdatingLocation()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }

    //Mark: MapView Delegates --------------------------------
    
    @objc func showTurnOnLocationServiceAlert(_ notification: NSNotification){
        let alert = UIAlertController(title: "Turn on Location Service", message: "To use location tracking feature of the app, please turn on the location service from the Settings app.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @objc func updateMap(_ notification: NSNotification){
        
        if let userInfo = notification.userInfo{
            //Todo; not working here at start.
            self.didInitialZoom = true
            updatePolylines()
            if let newLocation = userInfo["location"] as? CLLocation{
                zoomTo(location: newLocation)
            }
            
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay === self.accuracyRangeCircle{
            let circleRenderer = MKCircleRenderer(circle: overlay as! MKCircle)
            circleRenderer.fillColor = UIColor(white: 0.0, alpha: 0.25)
            circleRenderer.lineWidth = 0
            return circleRenderer
        }else{
            let polylineRenderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
            polylineRenderer.strokeColor = UIColor.black
            polylineRenderer.alpha = 0.5
            polylineRenderer.lineWidth = 5.0
            return polylineRenderer
        }
    }
    
    func updatePolylines(){
        var coordinateArray = [CLLocationCoordinate2D]()
        
        for loc in LocationService.sharedInstance.locationDataArray{
            coordinateArray.append(loc.coordinate)
        }
        
        self.clearPolyline()
        
        self.polyline = MKPolyline(coordinates: coordinateArray, count: coordinateArray.count)
        self.mapView.add(polyline as! MKOverlay)

        print("Total Distance:" + "\(LocationService.sharedInstance.traveledDistance/1000)"  + " " + "Km")
        print("Straight Distance:" + "\(LocationService.sharedInstance.straightDistance/1000)" + " " + "Km")
        print("Last Update:" + "\(LocationService.sharedInstance.LastUpdate)")
    }
    
    
    func clearPolyline(){
        if self.polyline != nil{
            self.mapView.remove(self.polyline!)
            self.polyline = nil
        }
    }
    
    func zoomTo(location: CLLocation){
        if self.didInitialZoom == false{
            /*let coordinate = location.coordinate
            let region = MKCoordinateRegionMakeWithDistance(coordinate, 300, 300)
            self.mapView.setRegion(region, animated: false) */
        }
        
        if self.isBlockingAutoZoom == false{
            self.isZooming = true
            self.mapView.setCenter(location.coordinate, animated: true)
        }
        
        var accuracyRadius = 50.0
        if location.horizontalAccuracy > 0{
            if location.horizontalAccuracy > accuracyRadius{
                accuracyRadius = location.horizontalAccuracy
            }
        }
        
        self.mapView.remove(self.accuracyRangeCircle!)
        self.accuracyRangeCircle = MKCircle(center: location.coordinate, radius: accuracyRadius as CLLocationDistance)
        self.mapView.add(self.accuracyRangeCircle!)
    }
    
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        if self.isZooming == true{
            self.isZooming = false
            self.isBlockingAutoZoom = false
        }else{
            self.isBlockingAutoZoom = true
            if let timer = self.zoomBlockingTimer{
                if timer.isValid{
                    timer.invalidate()
                }
            }
            if #available(iOS 10.0, *) {
                self.zoomBlockingTimer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false, block: { (Timer) in
                    self.zoomBlockingTimer = nil
                    self.isBlockingAutoZoom = false;
                })
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    //Mark: Helper Methods --------------------------------
    
    @objc func appWillEnterForeground(){
        if isTabBarVisible == false {
            DispatchQueue.main.async(execute: {
                self.view.frame = self.newTabBarSize!
                self.view.setNeedsDisplay()
                self.view.layoutIfNeeded()
            })
        }
    }

}

