//
//  LoginSignupViewController.swift
//  Ridelytic
//
//  Created by MyCandy on 11/07/17.
//  Copyright © 2017 Silver Seahog. All rights reserved.
//

import UIKit

class LandingViewController: UIViewController {
    
    @IBOutlet weak var jLoginBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        assignbackground()
        self.automaticallyAdjustsScrollViewInsets = false
        self.setNeedsStatusBarAppearanceUpdate()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
        
    func assignbackground(){
        let background = UIImage(named: "cyclistBg")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        self.view.insertSubview(imageView, at: 0)
    }
    
    @IBAction func loginSegueBtn(_ sender: UIButtonDeviceClass) {
        
        
    }
    @IBAction func loginTouchDown(_ sender: UIButton) {
        
        animateButtonOnPress(button: jLoginBtn)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    
}

