//
//  Cell.swift
//  Ridelytic Beta
//
//  Created by Satnam on 13/02/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit
class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabelDeviceClass!
    @IBOutlet weak var stats: UILabelDeviceClass!
    @IBOutlet weak var time: UILabelDeviceClass!
}
