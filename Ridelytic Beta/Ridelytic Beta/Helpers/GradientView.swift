//
//  GradientView.swift
//  Ridelytic Beta
//
//  Created by Satnam on 24/02/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class MapViewOverlay: UIView {
    
    //Passthrough
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return subviews.contains(where: {
            !$0.isHidden && $0.point(inside: self.convert(point, to: $0), with: event)
        })
    }
    
    //Gradient
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.locations = [0.0, 0.75]
        gradientLayer.colors = [
            UIColor.init(white: 1, alpha: 0).cgColor,
            UIColor.white.cgColor
        ]
        backgroundColor = UIColor.clear
    }
}

//
class TappableNavigationBar: UINavigationBar {
    var viewsToIgnore = [UIView]()
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        
        let ignore = viewsToIgnore.first {
            let converted = $0.convert(point, from: self)
            return $0.point(inside: converted, with: event)
        }
        return ignore == nil && super.point(inside: point, with: event)
    }
}

class PreviewMapViewOverlay: UIView {
    
    //Passthrough
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return subviews.contains(where: {
            !$0.isHidden && $0.point(inside: self.convert(point, to: $0), with: event)
        })
    }
    
    //Gradient
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.locations = [0.0, 0.95]
        gradientLayer.colors = [
            UIColor.init(white: 1, alpha: 0).cgColor,
            UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
        ]
        backgroundColor = UIColor.clear
    }
}
