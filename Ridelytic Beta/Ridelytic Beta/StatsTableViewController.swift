//
//  StatsTableViewController.swift
//  Ridelytic Beta
//
//  Created by Satnam Sync on 5/23/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit
import Charts
class StatsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
     //   self.tableView.separatorColor = UIColor.blue
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 315
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.row {
        case 0:
            let cell = Bundle.main.loadNibNamed("SpeedLineMapCell", owner: self, options: nil)?.first as! SpeedLineMapCell
            
            cell.yValues = [23, 29, 17, 16, 25, 69, 60, 53, 45, 2, 11, 48, 8, 93, 46, 38, 72, 42, 37, 27, 24, 79, 19, 62, 0, 97, 67, 70, 36, 41, 43, 40, 80, 89, 56, 90, 99, 63, 13, 51, 74, 94, 50, 32, 64, 66, 68, 18, 54, 73, 14, 26, 29, 49, 6, 47, 39, 83, 84, 92, 34, 65, 52, 82, 57, 1, 33, 16, 96, 10, 31, 77, 91, 87, 7, 81, 9, 85, 17, 5, 44, 86, 21, 22, 23, 15, 55, 25, 59, 98, 71, 95, 88, 28, 3, 30, 58, 35, 78, 4, 8, 4]
        
            cell.xValues = ["0 km", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100"]
            
            // if zero -> throws error
            
            cell.chtChart.xAxis.avoidFirstLastClippingEnabled = true
            
            let ll2 = ChartLimitLine(limit: Double(average(nums: cell.yValues)), label: "Avg Speed \(average(nums: cell.yValues)) ")
            print(cell.yValues)
            ll2.lineWidth = 1
            ll2.lineDashLengths = [5, 5]
            ll2.labelPosition = .rightBottom
            ll2.valueFont = UIFont(name: "AvenirNext-Medium", size: 13.0)!
            cell.chtChart.leftAxis.addLimitLine(ll2)
            cell.chtChart.leftAxis.drawLimitLinesBehindDataEnabled = true
           // cell.chtChart.fitScreen()
         //   cell.chtChart.
        
            
          //  cell.chtChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: cell.xValues)
            
            cell.setChart(dataPoints: cell.xValues, values: cell.yValues)
            cell.chtChart.setVisibleXRangeMaximum(Double(20.0))
            
           return cell
        case 1:
            let cell = Bundle.main.loadNibNamed("SplitsTableViewCell", owner: self, options: nil)?.first as! SplitsTableViewCell
            cell.unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.08]
            
            // Throws error without -1
            cell.months = Array(0...cell.unitsSold.count - 1).map{String($0)}
            print(cell.months)
            cell.months[0] = "\(cell.months[0] + " km")"
            cell.setChart(dataEntryX: cell.months, dataEntryY: cell.unitsSold)
            //  cell.viewForChart.xAxis.granularity = 1
          //  cell.viewForChart.xAxis.granularityEnabled = true
         //   cell.viewForChart.xAxis.labelCount = cell.unitsSold.count // number of points on X axis
         //   cell.viewForChart.setVisibleXRangeMaximum(Double(1))
         //   cell.viewForChart.setVisibleXRangeMinimum(Double(2.5))
            cell.viewForChart.setVisibleXRangeMaximum(Double(8.0))
            return cell
        default:
        let cell = Bundle.main.loadNibNamed("SpeedLineMapCell", owner: self, options: nil)?.first as! SpeedLineMapCell
            return cell
        }

        }
    func average(nums: [Double]) -> Int {
        
        var total = 0
        //use the parameter-array instead of the global variable votes
        for vote in nums{
            
            total += Int(vote)
            
        }
        
        let votesTotal = Int(nums.count)
        var average = total/votesTotal
        return average
    }

}
