//
//  TextDetailsCell.swift
//  Ridelytic Beta
//
//  Created by Satnam Sync on 4/21/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class TextDetailsCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
                
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
