//
//  ImageDetailsCell.swift
//  Ridelytic Beta
//
//  Created by Satnam Sync on 4/21/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class ImageDetailsCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        img.image = UIImage(named: "sampleImageTwo")
        img.layer.cornerRadius = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
