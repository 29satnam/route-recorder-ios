//
//  MapDetailsCell.swift
//  Ridelytic Beta
//
//  Created by Satnam Sync on 4/25/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class MapDetailsCell: UITableViewCell {

    @IBOutlet weak var mapImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mapImg.image = #imageLiteral(resourceName: "sampleImageThree")

    /*    mapImg.layer.masksToBounds = false
        mapImg.layer.shadowColor = UIColor.black.cgColor
        mapImg.layer.shadowOpacity = 0.19
        mapImg.layer.shadowOffset = CGSize(width: 0, height: 8)
        mapImg.layer.shadowRadius = 8
        
        mapImg.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        mapImg.layer.shouldRasterize = true
        mapImg.layer.rasterizationScale = true ? UIScreen.main.scale : 1
        */
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension UIView {
    func addShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.19
        self.layer.shadowRadius = 4
        self.layer.shadowOffset = CGSize(width: 8, height: 1)
    }
}
