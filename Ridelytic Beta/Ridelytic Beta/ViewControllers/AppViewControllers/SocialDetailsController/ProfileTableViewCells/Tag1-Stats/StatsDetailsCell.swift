//
//  StatsDetailsCell.swift
//  Ridelytic Beta
//
//  Created by Satnam Sync on 4/24/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class StatsDetailsCell: UITableViewCell {
    @IBOutlet weak var subView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      //  subView.addShadow()
        subView.layer.cornerRadius = 6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
      //  contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(0, 16, 0, 16))
    }

}

