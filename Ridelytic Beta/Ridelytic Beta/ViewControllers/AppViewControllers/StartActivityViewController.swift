//
//  StartActivityViewController.swift
//  Ridelytic Beta
//
//  Created by Satnam on 15/02/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit
import MapKit

class StartActivityViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var statsView: UIView!
    
    @IBOutlet weak var changeViewBtn: UIButton!
    @IBOutlet weak var routeBtn: UIButton!
    
    @IBOutlet weak var recenterBtn: UIButton!
    
    @IBOutlet weak var mapGradientView: UIView!
    
    
    @IBOutlet weak var startActivityBtn: UIButton!
    
    //Change Button Image and with Transition
    @IBAction func changeView(_ sender: UIButton) {
        if (mapView.isHidden == false || statsView.isHidden == true) {
            show(Map: true, Stats: false)
            
            UIView.transition(with: self.changeViewBtn,
                              duration:0.25,
                              options: .transitionCrossDissolve,
                              animations: { self.changeViewBtn.setImage(#imageLiteral(resourceName: "statsView"), for: UIControlState.normal) },
                              completion: nil)
        } else if (mapView.isHidden == true || statsView.isHidden == false) {
            show(Map: false, Stats: true)

            UIView.transition(with: self.changeViewBtn,
                              duration:0.25,
                              options: .transitionCrossDissolve,
                              animations: { self.changeViewBtn.setImage(#imageLiteral(resourceName: "mapView"), for: UIControlState.normal) },
                              completion: nil)
        }
    }

    //Views Transition
    @objc func show(Map: Bool, Stats: Bool) {
        let transitionOptions: UIViewAnimationOptions = [.transitionCrossDissolve, .showHideTransitionViews]
        UIView.transition(with: self.mapView, duration: 0.25, options: transitionOptions, animations: {
            self.mapView.isHidden = Map
        })
        
        UIView.transition(with: self.statsView, duration: 0.25, options: transitionOptions, animations: {
            self.statsView.isHidden = Stats
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        statsView.isHidden = true
        mapView.addSubview(mapGradientView)
        mapView.addSubview(routeBtn)
        mapView.addSubview(recenterBtn)
        self.view.addSubview(startActivityBtn)
        //startActivityBtn.backgroundColor = UIColor(red: 239.0/255, green:74.0/255, blue: 94.0/255, alpha: 1.0)
        
    }
    
    //Dismiss this modal controller
    @IBAction func dismissModal(_ sender: UIButton) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    
    //Show NavBar on next controller
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    //Hide NavBar on this controller
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        startActivityBtn.backgroundColor = UIColor(red: 239.0/255, green:74.0/255, blue: 94.0/255, alpha: 1.0)

    }
}
