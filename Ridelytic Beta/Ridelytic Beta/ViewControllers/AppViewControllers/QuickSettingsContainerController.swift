//
//  QuickSettingsTableViewController.swift
//  Ridelytic Beta
//
//  Created by Satnam on 25/02/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class QuickSettingsContainerController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
