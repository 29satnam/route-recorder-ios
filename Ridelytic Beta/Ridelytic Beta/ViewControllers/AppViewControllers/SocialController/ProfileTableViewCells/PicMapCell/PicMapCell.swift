//
//  PicMapCell.swift
//  Ridelytic Beta
//
//  Created by Satnam on 30/03/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class PicMapCell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var map: UIImageView!
    @IBOutlet weak var overlayView: PreviewMapViewOverlay!
    @IBOutlet weak var statsLabel: UILabelDeviceClass!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        img.image = #imageLiteral(resourceName: "sampleImage")
        map.image = #imageLiteral(resourceName: "sampleMap")
        
        img.layer.cornerRadius = 5
        map.layer.cornerRadius = 5
        
        img.layer.masksToBounds = true
        map.layer.masksToBounds = true
        
        img.layer.shouldRasterize = true
        map.layer.shouldRasterize = true
        
        
        map.addSubview(overlayView)
        overlayView.addSubview(statsLabel)
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

