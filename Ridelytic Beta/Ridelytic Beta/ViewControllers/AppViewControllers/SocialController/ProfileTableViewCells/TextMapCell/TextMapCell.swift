//
//  TextMapCell.swift
//  Ridelytic Beta
//
//  Created by Satnam on 30/03/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class TextMapCell: UITableViewCell {
    @IBOutlet weak var desc: UILabelDeviceClass!
    @IBOutlet weak var map: UIImageView!
    @IBOutlet weak var overlayView: PreviewMapViewOverlay!
    @IBOutlet weak var statsLabel: UILabelDeviceClass!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        map.image = #imageLiteral(resourceName: "sampleMap")
        map.layer.cornerRadius = 5
        map.layer.masksToBounds = true
        map.layer.shouldRasterize = true
        
        
        map.addSubview(overlayView)
        overlayView.addSubview(statsLabel)
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
