//
//  TextImageCell.swift
//  Ridelytic Beta
//
//  Created by Satnam on 30/03/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class TextPicCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        img.image = #imageLiteral(resourceName: "sampleImage")
        
        img.layer.cornerRadius = 5
        img.layer.masksToBounds = true
        img.layer.shouldRasterize = true 
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
