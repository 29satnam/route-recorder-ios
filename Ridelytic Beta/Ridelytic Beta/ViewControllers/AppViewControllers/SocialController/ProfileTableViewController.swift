//
//  ProfileTableViewController.swift
//  Ridelytic Beta
//
//  Created by Satnam on 28/02/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {
    var segmentedControl = UISegmentedControl()
    let buttonBar = UIView()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.view.backgroundColor = .white
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

        self.navigationController?.navigationItem.title = "Social"
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }

    // Dummy Data Online-Friends
    var avatars: [UIImage] = [
        UIImage(named: "avatarA")!,
        UIImage(named: "avatarB")!,
        UIImage(named: "avatarC")!,
        UIImage(named: "avatarD")!,
        UIImage(named: "avatarE")!,
        UIImage(named: "avatarB")!,
        UIImage(named: "avatarC")!,
        UIImage(named: "avatarD")!,
        UIImage(named: "avatarE")!
    ]
    var usernames: [String] = [
        "Christel",
        "Willia",
        "Tammy",
        "Darcey",
        "Russell",
        "Stormy",
        "Angele",
        "Dixie",
        "David"]
    
    var storedOffsets = [Int: CGFloat]()

    
    // Detect segmented control change.
    @objc func segmentValChanged(_ sender: UISegmentedControl) {
        print("tab", segmentedControl.tag)
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            print("First Segment Selected")
        case 1:
            print("Second Segment Selected")
        default:
            break
        }
        
        UIView.animate(withDuration: 0.3) {
            self.buttonBar.frame.origin.x = (self.segmentedControl.frame.width / CGFloat(self.segmentedControl.numberOfSegments)) * CGFloat(self.segmentedControl.selectedSegmentIndex)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OnlineFriendsTableRow", for: indexPath)
            return cell
        case 1:
            switch indexPath.row {
            case 0:
                let cell = Bundle.main.loadNibNamed("TextCell", owner: self, options: nil)?.first as! TextCell
                return cell
            case 1:
                let cell = Bundle.main.loadNibNamed("TextPicMapCell", owner: self, options: nil)?.first as! TextPicMapCell
                return cell
            case 2:
                let cell = Bundle.main.loadNibNamed("PicMapCell", owner: self, options: nil)?.first as! PicMapCell
                return cell
            case 3:
                let cell = Bundle.main.loadNibNamed("TextPicCell", owner: self, options: nil)?.first as! TextPicCell
                return cell
            case 4:
                let cell = Bundle.main.loadNibNamed("PicCell", owner: self, options: nil)?.first as! PicCell
                return cell
            case 5:
                let cell = Bundle.main.loadNibNamed("TextMapCell", owner: self, options: nil)?.first as! TextMapCell
                return cell
            case 6:
                let cell = Bundle.main.loadNibNamed("MapCell", owner: self, options: nil)?.first as! MapCell
                return cell
            default:
                let cell = Bundle.main.loadNibNamed("TextCell", owner: self, options: nil)?.first as! TextCell
                return cell
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OnlineFriendsTableRow", for: indexPath)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            guard let tableViewCell = cell as? OnlineFriendsTableRow else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        } else {
        }
    }

    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 28
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        return 7
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 100
        } else {
            return UITableViewAutomaticDimension
        }
        
    }
    
    

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        if section == 1 {
            //Add Segmented Control to header view.
            let header : UIView = UIView()
            header.backgroundColor = UIColor.white
            segmentedControl = UISegmentedControl(items: ["Friends", "Me"])
            segmentedControl.selectedSegmentIndex = 0
            segmentedControl.translatesAutoresizingMaskIntoConstraints = false
            segmentedControl.backgroundColor = .clear
            segmentedControl.tintColor = .clear
            segmentedControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.black], for: UIControlState.normal)
            segmentedControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.black], for: UIControlState.selected)
            buttonBar.translatesAutoresizingMaskIntoConstraints = false
            buttonBar.backgroundColor = UIColor.black
            header.addSubview(segmentedControl)
            segmentedControl.addSubview(buttonBar)
            print("bbb", header.bounds)
            
            
            buttonBar.bottomAnchor.constraint(equalTo: segmentedControl.bottomAnchor).isActive = true
            buttonBar.heightAnchor.constraint(equalToConstant: self.view.bounds.height*0.002).isActive = true
            buttonBar.leftAnchor.constraint(equalTo: segmentedControl.leftAnchor).isActive = true
            buttonBar.widthAnchor.constraint(equalTo: segmentedControl.widthAnchor, multiplier: 1 / CGFloat(segmentedControl.numberOfSegments)).isActive = true
            
            segmentedControl.addTarget(self, action: #selector(segmentValChanged(_:)), for: UIControlEvents.valueChanged)
            segmentedControl.leftAnchor.constraint(equalTo: header.leftAnchor, constant: self.view.bounds.width*0.04).isActive = true
            
            return header
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "postDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "postDetails" {
            let destinationController = segue.destination as! PostPreviewTableViewController
            destinationController.hidesBottomBarWhenPushed = true
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
}

extension ProfileTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return avatars.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnlineFriendsCollectionCells", for: indexPath) as! OnlineFriendsCollectionViewCell
        cell.avatar.image = self.avatars[indexPath.item]
        cell.username.text = self.usernames[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
