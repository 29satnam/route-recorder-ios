//
//  OnlineFriendsCells.swift
//  Ridelytic Beta
//
//  Created by Satnam on 31/03/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class OnlineFriendsTableRow: UITableViewCell {
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
   // @IBOutlet weak var avatar: UIImageView!
   // @IBOutlet weak var username: UILabelDeviceClass!
}


extension OnlineFriendsTableRow {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}

