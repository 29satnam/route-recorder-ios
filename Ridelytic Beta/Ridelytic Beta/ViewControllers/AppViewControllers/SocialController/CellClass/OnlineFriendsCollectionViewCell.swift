//
//  OnlineFriendsCollectionViewCell.swift
//  Ridelytic Beta
//
//  Created by Satnam Sync on 4/18/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit

class OnlineFriendsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var username: UILabelDeviceClass!
    
}
