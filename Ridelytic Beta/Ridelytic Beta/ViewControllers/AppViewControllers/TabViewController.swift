//
//  TabViewController.swift
//  Ridelytic Beta
//
//  Created by Satnam on 15/02/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit


class TabViewController: UITabBarController, UITabBarControllerDelegate {
    
    var startActivityRef: StartActivityViewController? // strong reference here
    @IBOutlet var rippleImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
//
    }
    
    // ProfileTableViewController - Check if loaded then scroll to top
    private var previousController: UINavigationController? = nil;
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("previousController:", previousController.debugDescription)
        
        guard let navigationController = tabBarController.viewControllers?[tabBarController.selectedIndex] as? UINavigationController else {
            return
        }
        
        let canScrollToTop = navigationController == previousController
        
        if let tableViewController = navigationController.topViewController as? ProfileTableViewController, canScrollToTop {

            tableViewController.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)

        }
        print("navigationController:", navigationController.debugDescription)

        previousController = navigationController
    }
    
    // Start Activity - return false / else true for rest of controllers
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is StartActivityViewController {
            if let modalView = self.storyboard?.instantiateViewController(withIdentifier: "StartActivityViewController") as? StartActivityViewController {
                
                if self.startActivityRef == nil {
                    self.startActivityRef = modalView // Check if it's already presenting/held in strong reference
                }
                
                let navController = UINavigationController(navigationBarClass: TappableNavigationBar.self, toolbarClass: nil)
                navController.setViewControllers([startActivityRef!], animated: true)
                navController.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                navController.navigationBar.shadowImage = UIImage()
                navController.navigationBar.isTranslucent = true
                navController.navigationBar.backgroundColor = UIColor.clear
                navController.navigationBar.isHidden = true
                self.present(navController, animated:true, completion: nil)
                return false
            }
        }
        return true
    }
    
    // Closure for ripple effect
    fileprivate func setupRippleBtn() {
        tabBar.insertSubview(rippleImg, at: 0)
        rippleImg.translatesAutoresizingMaskIntoConstraints = false
        rippleImg.centerXAnchor.constraint(equalTo: tabBar.centerXAnchor).isActive = true
        let verticalConstraint = NSLayoutConstraint(item: self.rippleImg, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.tabBar, attribute: NSLayoutAttribute.centerY, multiplier: 0.8375, constant: 0)
        NSLayoutConstraint.activate([verticalConstraint])
        
        UIView.animate(withDuration: 1, delay: 1, options: [.repeat, .curveEaseOut], animations: {
            self.rippleImg.alpha = 0.0
        }, completion: nil)
    }


}

