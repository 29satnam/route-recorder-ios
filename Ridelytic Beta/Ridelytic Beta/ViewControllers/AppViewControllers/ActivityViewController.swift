//
//  ActivityViewController.swift
//  Ridelytic Beta
//
//  Created by Satnam on 13/02/18.
//  Copyright © 2018 Ridelytic. All rights reserved.

import UIKit

class ActivityViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // collectionView.register(UINib.init(nibName: "Cell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        }
        collectionView.dataSource = self
        
    }
    
    
    @IBAction func activityAction(_ sender: UIButton) {
        print("tapped", UIScreen.main.bounds)
        
    }
    
    
   // let cell: MyCollectionViewCell = MyCollectionViewCell()
    let reuseIdentifier = "cell"
    var items = ["Morning Walk", "Evening Walk", "Morning Walk", "Marathon", "Evening Cycle", "Morning Cycle"]
    var stats = ["4.6 KM  5:11/KM  0.23", "4.6 KM  5:11/KM  0.23", "4.6 KM  5:11/KM  0.23", "4.6 KM  5:11/KM  0.23", "4.6 KM  5:11/KM  0.23", "4.6 KM  5:11/KM  0.23"]
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! MyCollectionViewCell
        cell.title.text = self.items[indexPath.item]
        cell.stats.text = self.stats[indexPath.item]

        return cell
    }
    
    // Auto-sizing collection view cells
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellsAcross: CGFloat = 3
        let spaceBetweenCells: CGFloat = 1
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        print("dim:", dim)
        return CGSize(width: dim, height: collectionView.bounds.height)
    }
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }

}
