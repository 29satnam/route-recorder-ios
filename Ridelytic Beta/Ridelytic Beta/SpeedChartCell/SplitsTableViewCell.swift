//
//  SplitsTableViewCell.swift
//  Ridelytic Beta
//
//  Created by Satnam on 6/5/18.
//  Copyright © 2018 Ridelytic. All rights reserved.
//

import UIKit
import Charts

class SplitsTableViewCell: UITableViewCell {

    @IBOutlet var viewForChart: BarChartView!
    var months: [String]!
    var unitsSold = [Double]()
    weak var axisFormatDelegate: IAxisValueFormatter?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewForChart.rightAxis.enabled = false
        self.viewForChart.rightAxis.drawAxisLineEnabled = false
        self.viewForChart.xAxis.labelPosition = .bottom
        self.viewForChart.xAxis.enabled = true
        self.viewForChart.xAxis.drawAxisLineEnabled = true
        self.viewForChart.xAxis.drawGridLinesEnabled = true
        self.viewForChart.xAxis.axisLineColor = UIColor.clear
        self.viewForChart.xAxis.labelFont = UIFont(name: "AvenirNext-Regular", size: 12.0)!
        self.viewForChart.xAxis.labelTextColor = UIColor(red: 20/255, green: 34/255, blue: 60/255, alpha: 1.0)
        
        self.viewForChart.leftAxis.enabled = true
        self.viewForChart.leftAxis.drawAxisLineEnabled = false
        self.viewForChart.leftAxis.labelFont = UIFont(name: "AvenirNext-Medium", size: 14.0)!
        self.viewForChart.leftAxis.labelTextColor = UIColor(red: 92/255, green: 92/255, blue: 92/255, alpha: 1.0)
        
        self.viewForChart.legend.enabled = false
        self.viewForChart.chartDescription?.text = ""
    //    self.viewForChart.leftAxis.drawLabelsEnabled = false
        self.viewForChart.xAxis.drawGridLinesEnabled = false
       // self.viewForChart.xAxis.labelText
        self.viewForChart.xAxis.granularityEnabled = true
       self.viewForChart.xAxis.granularity = 1
       // self.viewForChart.drawValueAboveBarEnabled = false
        self.viewForChart.leftAxis.axisDependency
    }

    func setChart(dataEntryX forX:[String],dataEntryY forY: [Double]) {
        viewForChart.noDataText = "You need to provide data for the chart."
        var dataEntries:[BarChartDataEntry] = []
        for i in 0..<forX.count{
            let dataEntry = BarChartDataEntry(x: Double(i), y: Double(forY[i]) , data: months as AnyObject?)
            print(dataEntry)
            dataEntries.append(dataEntry)
        }
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
        chartDataSet.colors = [NSUIColor(red: 255/255, green: 171/255, blue: 171/255, alpha: 1.0)]
        chartDataSet.barBorderWidth = 2
        chartDataSet.barBorderColor = NSUIColor(red: 239/255, green: 75/255, blue: 94/255, alpha: 1.0)
        let chartData = BarChartData(dataSet: chartDataSet)
        viewForChart.data = chartData
        viewForChart.data?.setDrawValues(false)
        viewForChart.data?.highlightEnabled = false
        let xAxisValue = viewForChart.xAxis
        xAxisValue.valueFormatter = axisFormatDelegate
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension SplitsTableViewCell: IAxisValueFormatter {
    
   func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return months[Int(value)]
    }
}
