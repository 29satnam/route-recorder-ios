//
//  StatsViewController
//  Ridelytic Beta
//
//  Created by Satnam Singh on 05/07/2018.
//  Copyright © 2018 Satnam Singh. All rights reserved.
//

import UIKit
import Charts

class StatsViewController: UIViewController {
    
    
    var months: [String]!
    var unitsSold = [Double]()
    weak var axisFormatDelegate: IAxisValueFormatter?
    
    @IBOutlet var viewForChart: BarChartView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        axisFormatDelegate = self
        
        unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0, 20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]

        months = Array(1...unitsSold.count - 1).map{String($0)}
        
        months[0] = "\(months[0] + " km")"
        
        viewForChart.scaleXEnabled = false
        viewForChart.scaleYEnabled = false
        // =---
        self.viewForChart.rightAxis.enabled = false
        self.viewForChart.rightAxis.drawAxisLineEnabled = false
        
        self.viewForChart.xAxis.drawGridLinesEnabled = false
        self.viewForChart.xAxis.axisLineColor = UIColor.clear
        self.viewForChart.xAxis.labelFont = UIFont(name: "AvenirNext-Regular", size: 12.0)!
        self.viewForChart.xAxis.labelTextColor = UIColor(red: 20/255, green: 34/255, blue: 60/255, alpha: 1.0)


        self.viewForChart.leftAxis.enabled = true
        self.viewForChart.leftAxis.drawAxisLineEnabled = false
        self.viewForChart.leftAxis.labelFont = UIFont(name: "AvenirNext-Medium", size: 14.0)!
        self.viewForChart.leftAxis.labelTextColor = UIColor(red: 92/255, green: 92/255, blue: 92/255, alpha: 1.0)
        
        self.viewForChart.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.viewForChart.legend.enabled = false
        self.viewForChart.chartDescription?.text = ""
        // =---
        setChart(dataEntryX: months, dataEntryY: unitsSold)
        
        
        
        self.viewForChart.xAxis.granularity = 1
        self.viewForChart.xAxis.granularityEnabled = true
        self.viewForChart.xAxis.labelCount = 8 // number of points on X axis
        
        viewForChart.setVisibleXRangeMaximum(Double(8))

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setChart(dataEntryX forX:[String],dataEntryY forY: [Double]) {
        viewForChart.noDataText = "You need to provide data for the chart."
        var dataEntries:[BarChartDataEntry] = []
        for i in 0..<forX.count{
            // print(forX[i])
            // let dataEntry = BarChartDataEntry(x: (forX[i] as NSString).doubleValue, y: Double(unitsSold[i]))
            let dataEntry = BarChartDataEntry(x: Double(i), y: Double(forY[i]) , data: months as AnyObject?)
            print(dataEntry)
            dataEntries.append(dataEntry)
        }
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
        let chartData = BarChartData(dataSet: chartDataSet)
        viewForChart.data = chartData
        let xAxisValue = viewForChart.xAxis
        xAxisValue.valueFormatter = axisFormatDelegate
        
    }
}

extension StatsViewController: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return months[Int(value)]
    }
}

